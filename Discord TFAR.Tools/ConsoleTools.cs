﻿using DSharpPlus;
using Microsoft.GotDotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public static class ConsoleTools
    {
        public static List<Action<ConsoleAction>> messages = new List<Action<ConsoleAction>>();

        public static List<Action<ConsoleAction>> GetLastMessages(int count)
        {
            List<Action<ConsoleAction>> result = new List<Action<ConsoleAction>>();

            if (count > messages.Count) count = messages.Count;

            result = messages.GetRange(messages.Count - count, count);

            return result;
        }

        public static void PrintToCenter(string message)
        {
            int width = Console.BufferWidth;
            int startpoint = 0;
            int length = 0;

            using (StringReader reader = new StringReader(message))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    if (line.Length > length) length = line.Length;
                }
            }

            using (StringReader reader = new StringReader(message))
            {
                double result = (width - length) / 2;
                startpoint = (int)Math.Floor(result);
                
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine($"{EmptySpaces(startpoint)}{line}");
                }
            }
        }

        internal static string EmptySpaces(int count)
        {
            string result = "";
            for(int i = 0; i < count; i++)
            {
                result += " ";
            }
            return result;
        }

        public static void WriteDebug(LogMessage m, string prefix)
        {
            Logger.LogMessages.Add($"[{m.Level} | {prefix.ToUpper()}] {m.Message}{Environment.NewLine}");

            if (m.Level == MessageLevel.Unecessary)
                return;

            ConsoleForeground fgColor = ConsoleForeground.White;
            ConsoleBackground bgColor = ConsoleBackground.Black;

            switch (m.Level)
            {
                case MessageLevel.Debug:
                    fgColor = ConsoleForeground.Green;
                    break;
                case MessageLevel.Error:
                    fgColor = ConsoleForeground.Red;
                    break;
                case MessageLevel.Critical:
                    fgColor = ConsoleForeground.White;
                    bgColor = ConsoleBackground.Red;
                    break;
                case MessageLevel.Warning:
                    fgColor = ConsoleForeground.Yellow;
                    break;
            }
            Action<ConsoleAction> message = new Action<ConsoleAction>((e) =>
            {
                ConsoleEx.TextColor(fgColor, bgColor);
                string pre = $"[{prefix.ToUpper()}: {m.TimeStamp}:{m.TimeStamp.Millisecond:000}]:";
                ConsoleEx.WriteAt(e.x, e.y, pre);
                ConsoleEx.TextColor(ConsoleForeground.White, ConsoleBackground.Black);
                string tMsg = m.Message;
                if ((pre + " " + m.Message).Length > Console.WindowWidth) tMsg = tMsg.Remove(Console.WindowWidth - (pre + " ").Length);
                ConsoleEx.WriteAt(e.x + pre.Length, e.y, " " + tMsg);
            });

            messages.Add(message);

            DrawLog();
        }

        public static void DrawConsole(Plugin plugin)
        {
            ConsoleEx.Title = "Discord TFAR Console";
            ConsoleEx.Clear();
            ConsoleEx.TextColor(ConsoleForeground.Black, ConsoleBackground.Cyan);
            ConsoleEx.DrawRectangle(BorderStyle.None, 0, 0, Console.WindowWidth-1, Console.WindowHeight-1, true);
            
            ConsoleEx.WriteAt(1, 0, "Discord TFAR Console");
            DrawStatusBar(plugin);
            DrawLog();
            DrawInput();
            DrawVoiceBar(plugin);

            Console.SetCursorPosition(0, 0);
            ConsoleEx.CursorVisible = false;
        }

        public static void DrawStatusBar(Plugin plugin)
        {
            ConsoleEx.TextColor(ConsoleForeground.Red, ConsoleBackground.Cyan);
            ConsoleEx.WriteAt((Console.WindowWidth - GetStatus(plugin.client.Me).Length) - 1, 0, GetStatus(plugin.client.Me));
            ConsoleEx.TextColor(ConsoleForeground.White, ConsoleBackground.Black);
        }

        public static void DrawLog()
        {
            ConsoleEx.TextColor(ConsoleForeground.Black, ConsoleBackground.Black);
            ConsoleEx.DrawRectangle(BorderStyle.None, 0, 1, Console.WindowWidth - 1, Console.WindowHeight - 5, true);
            int space = Console.WindowHeight - (3 + 2);
            List<Action<ConsoleAction>> entries = GetLastMessages(space);
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i] == null) continue;
                ConsoleEx.Move(0, i + 1);
                entries[i](new ConsoleAction() { x = 0, y = i + 1 });
            }

            DrawInput();
        }

        public static void DrawInput()
        {
            ConsoleEx.TextColor(ConsoleForeground.Black, ConsoleBackground.Black);
            ConsoleEx.DrawRectangle(BorderStyle.None, 0, Console.WindowHeight - 4, Console.WindowWidth -1, 0, true);

            ConsoleEx.TextColor(ConsoleForeground.White, ConsoleBackground.Black);
            ConsoleEx.WriteAt(1, Console.WindowHeight - 4, "> ");
            ConsoleEx.Move(3, Console.WindowHeight - 4);
            ConsoleEx.CursorVisible = true;
        }

        public static void DrawVoiceBar(Plugin plugin)
        {
            string serverName = "(None)";
            string channelName = "(None)";
            int serverCount = 0;
            int channelCount = 0;
            if (plugin.client.GetVoiceClient() != null && plugin.client.GetVoiceClient().Guild != null)
            {
                serverName = plugin.client.GetVoiceClient().Guild.Name;
                serverCount = plugin.client.GetVoiceClient().Guild.Members.Count;
            }
            if (plugin.client.GetVoiceClient() != null && plugin.client.GetVoiceClient().Channel != null)
            {
                channelName = plugin.client.GetVoiceClient().Channel.Name;
                channelCount = plugin.client.GetVoiceClient().Guild.Members.Values.Count(x => x.CurrentVoiceChannel.ID == plugin.client.GetVoiceClient().Channel.ID);
            }
            ConsoleEx.TextColor(ConsoleForeground.Black, ConsoleBackground.Cyan);
            ConsoleEx.WriteAt(1, Console.WindowHeight - 3, $"Server:");
            ConsoleEx.WriteAt(1, Console.WindowHeight - 2, $"Channel:");
            ConsoleEx.WriteAt(1, Console.WindowHeight - 1, $"Users (Channel/Server):");

            ConsoleEx.WriteAt((int)Console.WindowWidth / 2, Console.WindowHeight - 3, $"Players Muted:");
            ConsoleEx.WriteAt((int)Console.WindowWidth / 2, Console.WindowHeight - 2, $"Players Deafen:");
            ConsoleEx.WriteAt((int)Console.WindowWidth / 2, Console.WindowHeight - 1, $"Players Talking:");


            ConsoleEx.TextColor(ConsoleForeground.Red, ConsoleBackground.Cyan);
            ConsoleEx.WriteAt(25, Console.WindowHeight - 3, serverName);
            ConsoleEx.WriteAt(25, Console.WindowHeight - 2, channelName);
            ConsoleEx.WriteAt(25, Console.WindowHeight - 1, $"{channelCount}/{serverCount}");

            ConsoleEx.WriteAt((int)Console.WindowWidth / 2 + 17, Console.WindowHeight - 3, "0");
            ConsoleEx.WriteAt((int)Console.WindowWidth / 2 + 17, Console.WindowHeight - 2, "0");
            ConsoleEx.WriteAt((int)Console.WindowWidth / 2 + 17, Console.WindowHeight - 1, "0");
        }

        private static string GetStatusBar(Plugin plugin)
        {
            string title = " Discord TFAR Console";
            string status = GetStatus(plugin.client.Me);
            return $"{title}{EmptySpaces(Console.BufferWidth - (title.Length + status.Length))}{status}";
        }

        private static string GetStatus(DSharpPlus.Objects.DiscordMember member)
        {
            string result = "Muted | Deafen (Placeholder; No functionality)";
            if (member != null && member.Muted && member.Deaf) result = "Muted | Deafen";
            else if (member != null && member.Muted) result = "Muted";
            else if (member != null && member.Deaf) result = "Deafen";
            return result;
        }
    }

    public class ConsoleAction
    {
        public int x;
        public int y;
    }
}
