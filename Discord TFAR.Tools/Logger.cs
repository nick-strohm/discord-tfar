﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public static class Logger
    {
        public static List<string> LogMessages = new List<string>();
        private static bool stopLogging = false;

        public static void StartLogging()
        {
            BackgroundWorker bgWrk = new BackgroundWorker();
            bgWrk.DoWork += (sender, e) =>
            {
                DateTime time;
                while(!stopLogging)
                {
                    if (LogMessages.Count == 0) { System.Threading.Thread.Sleep(500); continue; }

                    time = DateTime.Now;

                    if (!Directory.Exists("log")) Directory.CreateDirectory("log");
                    if (!File.Exists($"log/{time.Year}-{time.Month}-{time.Day}.log"))
                    {
                        var file = File.Create($"log/{time.Year}-{time.Month}-{time.Day}.log");
                        file.Close();
                    }
                    File.AppendAllText($"log/{time.Year}-{time.Month}-{time.Day}.log", LogMessages[0]);
                    LogMessages.RemoveAt(0);
                    System.Threading.Thread.Sleep(100);
                }
            };
            bgWrk.RunWorkerAsync();
        }

        public static bool StopLogging()
        {
            while(LogMessages.Count > 0) { }
            stopLogging = true;
            return true;
        }
    }
}
