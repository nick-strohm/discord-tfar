﻿using DSharpPlus;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class LoginTools
    {
        public static bool DoesUserFileExists() { return File.Exists("user.dat"); }

        public static bool CanLogin(string salt)
        {
            try
            {
                JObject json = JObject.Parse(StringCipher.Decrypt(File.ReadAllText("user.dat"), salt));
                string email = json["email"].ToString();
                string pass = json["pass"].ToString();
                string token = DSharpPlus.Toolbox.Tools.getUserToken(email, pass);

                DiscordClient client = new DiscordClient(token, false, true);
                client.SendLoginRequest();
                client.Connect();
                client.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Save(string email, string pass, string salt)
        {
            try
            {
                string token = DSharpPlus.Toolbox.Tools.getUserToken(email, pass);

                DiscordClient client = new DiscordClient(token, false, true);
                client.SendLoginRequest();
                client.Connect();
                client.Dispose();
                File.WriteAllText("user.dat", StringCipher.Encrypt(new JObject() { { "email", email }, { "pass", pass } }.ToString(), salt));
                return true;
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public static string GetToken(string salt)
        {
            try
            {
                JObject json = JObject.Parse(StringCipher.Decrypt(File.ReadAllText("user.dat"), salt));
                string email = json["email"].ToString();
                string pass = json["pass"].ToString();
                string token = DSharpPlus.Toolbox.Tools.getUserToken(email, pass);

                DiscordClient client = new DiscordClient(token, false, true);
                client.SendLoginRequest();
                client.Connect();
                client.Dispose();
                return token;
            } catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
