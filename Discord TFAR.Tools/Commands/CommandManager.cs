﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Commands
{
    public class CommandManager
    {
        public Tools.Pipe pipe;
        private List<CommandStub> commands;

        public List<CommandStub> Commands
        {
            get
            {
                return commands;
            }
        }

        public CommandManager(Tools.Pipe pipe)
        {
            this.pipe = pipe;
            pipe.MessageReceived += (sender, e) =>
            {
                ExecuteCommand(e.Message);
            };
            commands = new List<CommandStub>();
        }

        public void Add(CommandStub command)
        {
            commands.Add(command);
            Tools.ConsoleTools.WriteDebug(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Debug, Message = $"Command {command.CommandName} added [{command.Aliases.Count} Aliases]", TimeStamp = DateTime.Now }, "PIPE");
        }

        public void ExecuteCommand(string RawCommand)
        {
            List<string> split = RawCommand.Split(new char[] { ' ' }).ToList();
            string cmdName = split[0];
            split.RemoveAt(0);

            CommandStub command = commands.Find(x => x.CommandName == cmdName);
            if (command == null) command = commands.Find(x => x.Aliases.Contains(cmdName));
            if (command != null) command.Trigger(pipe, cmdName, split.ToArray());
            else
            {
                pipe.Write("FAIL");
                return;
            }

            Tools.ConsoleTools.WriteDebug(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Debug, Message = $"Command {command.CommandName} executed", TimeStamp = DateTime.Now }, "PIPE");
        }
    }
}
