﻿using Discord_TFAR.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Commands
{
    public class CommandStub : ICommand
    {
        private string _commandName;
        private List<string> _aliases;
        private Action<CommandArgs> _commandFunc;

        public string CommandName
        {
            get
            {
                return _commandName;
            }

            set
            {
                _commandName = value;
            }
        }
        public List<string> Aliases
        {
            get
            {
                return _aliases;
            }
        }
        public Action<CommandArgs> CommandFunc
        {
            get
            {
                return _commandFunc;
            }

            set
            {
                _commandFunc = value;
            }
        }

        public CommandStub(string CommandName, Action<CommandArgs> CommandFunc)
        {
            this.CommandName = CommandName;
            this.CommandFunc = CommandFunc;
            _aliases = new List<string>();
        }

        public CommandStub AddAlias(string alias)
        {
            _aliases.Add(alias);
            return this;
        }

        public void Trigger(Tools.Pipe pipe, string command, string[] parameters)
        {
            CommandArgs e = new CommandArgs();
            e.Pipe = pipe;
            e.Parameters = parameters;
            e.UsedCommand = command;
            e.RawCommand = $"{command} {string.Join(" ", parameters)}";

            CommandFunc.Invoke(e);
        }
        public void Trigger(Tools.Pipe pipe, string command, string parameters)
        {
            Trigger(pipe, command, parameters.Split(new char[] { ' ' }));
        }
    }

    public class CommandArgs : EventArgs
    {
        public Tools.Pipe Pipe;
        public string[] Parameters;
        public string UsedCommand;
        public string RawCommand;
        public ulong CurrentServerConnectionHandlerID;
    }
}
