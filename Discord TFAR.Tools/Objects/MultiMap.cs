﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Objects
{
    public class MultiMap<V>
    {
        Dictionary<string, List<V>> _dictionary =
        new Dictionary<string, List<V>>();
        
        public void Add(string key, V value)
        {
            List<V> list;
            if (this._dictionary.TryGetValue(key, out list))
            {
                list.Add(value);
            }
            else
            {
                list = new List<V>();
                list.Add(value);
                this._dictionary[key] = list;
            }
        }
        
        public IEnumerable<string> Keys
        {
            get
            {
                return this._dictionary.Keys;
            }
        }
        
        public List<V> this[string key]
        {
            get
            {
                List<V> list;
                if (!this._dictionary.TryGetValue(key, out list))
                {
                    list = new List<V>();
                    this._dictionary[key] = list;
                }
                return list;
            }
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        public Dictionary<string, V> GetDictionary()
        {
            Dictionary<string, V> _dict = new Dictionary<string, V>();
            foreach (KeyValuePair<string, List<V>> vars in _dictionary)
                foreach (V var in vars.Value)
                    _dict.Add(vars.Key, var);

            return _dict;
        }
    }
}
