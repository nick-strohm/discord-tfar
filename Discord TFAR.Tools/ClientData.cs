﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class ClientData
    {
        private string _id = "";
        private string _name = "pX";
        private IrrKlang.Vector3D _position = new IrrKlang.Vector3D(0, 0, 0);
        private IntPtr _positionTime = new IntPtr(0);
        private IrrKlang.Vector3D _rotation = new IrrKlang.Vector3D(0, 0, 0);
        private bool _canSpeak = false;
        private bool _canUseSWRadio = false;
        private bool _canUseLRRadio = false;
        private bool _canUseDDRadio = false;
        private string _vehicleID = "";
        private int _terrainInterception = 0;
        private float _voiceVolume = 0.0f;
        private float _currentUnitDirection = 0.0f;

        public string ID { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public IrrKlang.Vector3D Position { get { return _position; } set { _position = value; } }
        public IntPtr PositionTime { get { return _positionTime; }set { _positionTime = value; } }
        public IrrKlang.Vector3D Rotation { get { return _rotation; } set { _rotation = value; } }
        public bool CanSpeak { get { return _canSpeak; } set { _canSpeak = value; } }
        public bool CanUseSWRadio { get { return _canUseSWRadio; } set { _canUseSWRadio = value; } }
        public bool CanUseLRRadio { get { return _canUseLRRadio; } set { _canUseLRRadio = value; } }
        public bool CanUseDDRadio { get { return _canUseDDRadio; } set { _canUseDDRadio = value; } }
        public string VehicleID { get { return _vehicleID; } set { _vehicleID = value; } }
        public int TerrainInterception { get { return _terrainInterception; } set { _terrainInterception = value; } }
        public float VoiceVolume { get { return _voiceVolume; } set { _voiceVolume = value; } }
        public float CurrentUnitDirection { get { return _currentUnitDirection; } set { _currentUnitDirection = value; } }
    }
}
