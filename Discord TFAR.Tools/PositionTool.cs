﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class PositionTool
    {
        public IrrKlang.Vector3D CalculateRelativePositionCounterClockwise(ClientData bPlayer, ClientData rPlayer)
        {
            IrrKlang.Vector3D bPosition = bPlayer.Position;
            IrrKlang.Vector3D rPosition = rPlayer.Position;
            float s = (float)Math.Sin(bPlayer.Rotation.Y);
            float c = (float)Math.Cos(bPlayer.Rotation.Y);

            bPosition -= rPlayer.Position;

            rPosition.X = (bPosition.X * c - bPosition.Y * s);
            rPosition.Y = (bPosition.X * s + bPosition.Y * c);
            
            return rPosition;
        }

        public IrrKlang.Vector3D CalculateRelativePositionClockwise(ClientData bPlayer, ClientData rPlayer)
        {
            IrrKlang.Vector3D bPosition = bPlayer.Position;
            IrrKlang.Vector3D rPosition = rPlayer.Position;
            float s = (float)Math.Sin(bPlayer.Rotation.Y);
            float c = (float)Math.Cos(bPlayer.Rotation.Y);

            bPosition -= rPlayer.Position;

            rPosition.X = (bPosition.X * c + bPosition.Y * s);
            rPosition.Y = (-bPosition.X * s + bPosition.Y * c);

            return rPosition;
        }

        public float Square(float x) { return x * x; }

        public float Distance(IrrKlang.Vector3D bPosition, IrrKlang.Vector3D rPosition)
        {
            return (float)Math.Sqrt(Square(bPosition.X - rPosition.X) + Square(bPosition.Y - rPosition.Y) + Square(bPosition.Z - rPosition.Z));
        }
    }
}
