﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class AudioTools
    {
        public event EventHandler<WaveInEventArgs> DataAvailable;
        protected virtual void OnDataAvailable(object sender, WaveInEventArgs e)
        {
            DataAvailable?.Invoke(sender, e);
        }

        private WaveIn waveSource = null;

        public AudioTools()
        {
            GetAudioDevices();
        }

        public List<WaveInCapabilities> GetAudioDevices()
        {
            List<WaveInCapabilities> devices = new List<WaveInCapabilities>();
            int waveInDevices = WaveIn.DeviceCount;
            for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                devices.Add(deviceInfo);
                Console.WriteLine("Device {0}: {1}, {2} channels", waveInDevice, deviceInfo.ProductName, deviceInfo.Channels);
            }
            return devices;
        }

        public void RecordAudio(int deviceNumber)
        {
            waveSource = new WaveIn();
            waveSource.DeviceNumber = deviceNumber;
            waveSource.WaveFormat = new WaveFormat(44100, 2);
            waveSource.DataAvailable += (sender, e) => OnDataAvailable(sender, e);
            waveSource.StartRecording();

            Console.WriteLine($"Started recording on device {deviceNumber}");
        }

        public void StopRecording()
        {
            waveSource?.StopRecording();

            Console.WriteLine($"Stopped recording");
        }
    }
}
