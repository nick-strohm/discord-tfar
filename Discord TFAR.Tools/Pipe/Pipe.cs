﻿using DSharpPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class Pipe
    {
        string networkHost = ".";
        string pipeName = "task_force_radio_pipe";
        NamedPipeClientStream pipeClient;
        StreamReader streamr;
        StreamWriter streamw;
        System.Threading.Thread t;

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        protected virtual void OnMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            MessageReceived?.Invoke(sender, e);
        }

        public Pipe()
        {
            pipeClient = new NamedPipeClientStream(networkHost, pipeName, PipeDirection.InOut, PipeOptions.Asynchronous, System.Security.Principal.TokenImpersonationLevel.Impersonation);
            t = new System.Threading.Thread(Listen);

            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Constructed", TimeStamp = DateTime.Now }, "PIPE");

        }

        private void Listen()
        {
            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Starting", TimeStamp = DateTime.Now }, "PIPE");
            pipeClient.Connect();
            if (pipeClient.IsConnected)
            {
                streamw = new StreamWriter(pipeClient);
                streamr = new StreamReader(pipeClient);
            }
            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Started", TimeStamp = DateTime.Now }, "PIPE");

            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Listening", TimeStamp = DateTime.Now }, "PIPE");
            while (pipeClient.IsConnected)
            {
                try
                {
                    string message = streamr.ReadLine();
                    Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Received:{Environment.NewLine}    {message}", TimeStamp = DateTime.Now }, "PIPE");

                    OnMessageReceived(this, new MessageReceivedEventArgs() { Message = message });

                    Write("OK");
                }
                catch
                {

                }
            }
        }

        public void Start()
        {
            t.Start();
        }

        public void Stop()
        {
            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Stopping", TimeStamp = DateTime.Now }, "PIPE");
            pipeClient.Close();
        }

        public void Write(string message)
        {
            char[] output = message.ToCharArray();

            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Sending: {output.ToString()} (Test: {output})", TimeStamp = DateTime.Now }, "PIPE");

            streamw.Write(output);
            streamw.Flush();
        }

        public bool IsConnected() => pipeClient.IsConnected;
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public string Message;
    }
}
