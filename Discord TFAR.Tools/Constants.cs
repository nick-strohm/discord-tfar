﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public static class Constants
    {
        public static int INVALID_DATA_FRAME = 9999;
        public static int PATH_BUFSIZE = 512;

        public static int DD_MIN_DISTANCE = 70;
        public static int DD_MAX_DISTANCE = 300;

        public static float UNDERWATER_LEVEL = -1.1f;
    }
}
