﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public class PTTDelayArguments
    {
        public string commandToBroadcast;
        public ulong currentServerConnectionHandlerID; // ServerID
        public string subtype;
    }
}
