﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public class SPEAKER_DATA
    {
        public string radio_id;
        public string nickname;
        public LinkedList<float> pos = new LinkedList<float>();
        public int volume;
        public KeyValuePair<string, float> vehicle = new KeyValuePair<string, float>();
        public float waveZ;
    }
}
