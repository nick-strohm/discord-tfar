﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public enum LISTED_ON
    {
        LISTED_ON_SW,
        LISTED_ON_LR,
        LISTED_ON_DD,
        LISTED_ON_NONE,
        LISTED_ON_GROUND
    }
}
