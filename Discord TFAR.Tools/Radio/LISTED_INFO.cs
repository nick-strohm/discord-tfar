﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public class LISTED_INFO
    {
        public OVER_RADIO_TYPE over;
        public LISTED_ON on;
        public int volume;
        public int stereoMode;
        public string radio_id;
        public IrrKlang.Vector3D pos;
        public float waveZ;
        public KeyValuePair<string, float> vehicle;
    }
}
