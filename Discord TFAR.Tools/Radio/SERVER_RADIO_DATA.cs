﻿using Discord_TFAR.Tools.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public class SERVER_RADIO_DATA
    {
        public string myNickname;
        public string myOriginalNickname;
        public bool tangentPressed;
        public IrrKlang.Vector3D myPosition;
        public STRING_TO_CLIENT_DATA_MAP nicknameToClientData;
        public SortedDictionary<string, FREQ_SETTINGS> mySwFrequencies;
        public SortedDictionary<string, FREQ_SETTINGS> myLrFrequencies;

        public string myDdFrequency;
        public MultiMap<SPEAKER_DATA> speakers;
        public SortedDictionary<string, object> waves;

        public int ddVolumeLevel;
        public int myVoiceVolume;
        public bool alive;
        public bool canSpeak;
        public float wavesLevel;
        public float terrainIntersectionCoefficient;
        public float globalVolume;
        public float receivingDistanceMultiplicator;
        public float speakersDistance;

        public string serious_mod_channel_name; // ServerID
        public string serious_mod_channel_password; // ChannelID
        public string addon_version;

        public int currentDataFrame;

        public SERVER_RADIO_DATA()
        {
            tangentPressed = false;
            currentDataFrame = Constants.INVALID_DATA_FRAME;
            terrainIntersectionCoefficient = 7.0f;
            globalVolume = receivingDistanceMultiplicator = 1.0f;
            speakersDistance = 20.0f;
        }
    }
}
