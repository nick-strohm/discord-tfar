﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public enum OVER_RADIO_TYPE
    {
        LISTEN_TO_SW,
        LISTEN_TO_LR,
        LISTEN_TO_DD,
        LISTEN_TO_NONE
    }
}
