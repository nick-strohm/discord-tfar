﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools.Radio
{
    public class CLIENT_DATA
    {
        public bool pluginEnabled;
        public IntPtr pluginEnabledCheck;
        public ulong clientId;
        public OVER_RADIO_TYPE tangentOverType;
        public IrrKlang.Vector3D clientPosition;
        public ulong positionTime;

        public string frequency;
        public int voiceVolume;
        public int range;

        public bool canSpeak;
        public bool clientTalkingNow;
        public int dataFrame;

        public bool canUseSWRadio;
        public bool canUseLRRadio;
        public bool canUseDDRadio;

        public string subtype;
        public string vehicleId;

        public int terrainInterception;

        public Dictionary<string, object> swEffects;
        public Dictionary<string, object> lrEffects;
        public Dictionary<string, object> ddEffects;
        public Dictionary<string, object> clunks;

        public Dictionary<string, object> filtersCantSpeak;
        public Dictionary<string, object> filtersVehicle;
        public Dictionary<string, object> filtersSpeakers;
        public Dictionary<string, object> filtersPhone;

        public float viewAngle;

        public float voiceVolumeMultiplier;

        public object getSpeakersPhone(string key)
        {
            if (filtersPhone.Count(x => x.Key == key) == 0)
            {
                filtersPhone[key] = new object();
            }
            return filtersPhone[key];
        }

        public object getSpeakersFilter(string key)
        {
            if (filtersSpeakers.Count(x => x.Key == key) == 0)
            {
                filtersSpeakers[key] = new object();
            }
            return filtersSpeakers[key];
        }

        public object getSwRadioEffect(string key)
        {
            if (swEffects.Count(x => x.Key == key) == 0)
            {
                swEffects[key] = new object();
            }
            return swEffects[key];
        }

        public object getLrRadioEffect(string key)
        {
            if (lrEffects.Count(x => x.Key == key) == 0)
            {
                lrEffects[key] = new object();
            }
            return lrEffects[key];
        }

        public object getUnderwaterRadioEffect(string key)
        {
            if (ddEffects.Count(x => x.Key == key) == 0)
            {
                ddEffects[key] = new object();
            }
            return ddEffects[key];
        }

        public object getClunk(string key)
        {
            if (clunks.Count(x => x.Key == key) == 0)
            {
                clunks[key] = new object();
            }
            return clunks[key];
        }

        public void removeClunk(string key)
        {
            if (clunks.Count(x => x.Key == key) > 0)
            {
                clunks.Remove(key);
            }
        }

        public object getFilterCantSpeak(string key)
        {
            if (filtersCantSpeak.Count(x => x.Key == key) == 0)
            {
                filtersCantSpeak[key] = new object();
            }
            return filtersCantSpeak[key];
        }

        public object getFilterVehicle(string key)
        {
            if (filtersVehicle.Count(x => x.Key == key) == 0)
            {
                filtersVehicle[key] = new object();
            }
            return filtersVehicle[key];
        }

        public object compressor;
        public void resetRadioEffect()
        {
            swEffects.Clear();
            lrEffects.Clear();
            ddEffects.Clear();
            filtersSpeakers.Clear();
            filtersPhone.Clear();
        }

        public void resetVoices()
        {
            clunks.Clear();
            filtersCantSpeak.Clear();
            filtersVehicle.Clear();
        }

        public CLIENT_DATA()
        {
            positionTime = 0;
            tangentOverType = OVER_RADIO_TYPE.LISTEN_TO_NONE;
            dataFrame = 0;
            clientPosition.X = clientPosition.Y = clientPosition.Z = 0;
            clientId = 0;
            voiceVolume = 0;
            canSpeak = true;
            canUseLRRadio = canUseSWRadio = canUseDDRadio = false;
            range = 0;
            terrainInterception = 0;
            voiceVolumeMultiplier = 1.0f;

            resetRadioEffect();
        }
    }
}
