﻿using Discord_TFAR.Interfaces;
using Discord_TFAR.Tools.Radio;
using DSharpPlus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class Plugin
    {
        internal Pipe pipe;
        internal Commands.CommandManager commandManager;
        internal DiscordClient client;

        internal List<CLIENT_DATA> players = new List<CLIENT_DATA>();
        internal Dictionary<string, bool> talking = new Dictionary<string, bool>();

        SERVER_ID_TO_SERVER_DATA serverIdToData = new SERVER_ID_TO_SERVER_DATA();
        bool skip_tangent_off = false;
        bool waiting_tangent_off = false;

        bool vadEnabled = false;

        PTTDelayArguments ptt_arguments;

        public Plugin(ref DiscordClient client)
        {
            pipe = new Tools.Pipe();
            commandManager = new Commands.CommandManager(pipe);
            this.client = client;
            this.client.GetTextClientLogger.LogMessageReceived += (sender, e) => ConsoleTools.WriteDebug(e.message, "DISCORD");
            this.client.MentionReceived += (sender, e) =>
            {
                if (e.Author.ID != "129303865161023488") return;

                this.client.DisconnectFromVoice();
            };
            this.client.UserSpeaking += (sender, e) =>
            {
                if (!talking.ContainsKey(e.UserSpeaking.ID))
                    talking.Add(e.UserSpeaking.ID, e.Speaking);
                else
                    talking[e.UserSpeaking.ID] = e.Speaking;
            };

            Commands();
        }

        public void Start()
        {
            client.SendLoginRequest();
            client.Connect();
            pipe.Start();
            Logger.StartLogging();

            Play2DAudio("radio-sounds/on.wav");
        }

        public void Stop()
        {
            client.DisconnectFromVoice();
            client.Dispose();
            pipe.Stop();
            if (Logger.StopLogging())
                Environment.Exit(0);
        }

        private void Play2DAudio(string file)
        {
            try
            {
                IrrKlang.ISoundEngine engine = new IrrKlang.ISoundEngine(IrrKlang.SoundOutputDriver.DirectSound8, IrrKlang.SoundEngineOptionFlag.DefaultOptions);
                engine.Play2D(file);

                Tools.ConsoleTools.DrawConsole(this);
            }
            catch { }
        }

        public void SoundCheck()
        {
            ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Soundcheck starting", TimeStamp = DateTime.Now }, "SYSTEM");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/lr/local_start.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/lr/local_end.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/dd/local_start.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/dd/local_end.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/sw/local_start.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/sw/local_end.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/ab/local_start.wav");
            System.Threading.Thread.Sleep(500);
            Play2DAudio("radio-sounds/ab/local_end.wav");
            ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Soundcheck done. 6 Sounds should have been played", TimeStamp = DateTime.Now }, "SYSTEM");
        }

        private int versionNumber(string versionString)
        {
            int number = 0;
            for(int q = 0; q < versionString.Length; q++)
            {
                char ch = versionString.ElementAt(q);
                if(isdigit(ch))
                {
                    number += (ch - 48);
                }
                if(ch == '.')
                {
                    number *= 10;
                }
            }
            return number;
        }

        private double parseArmaNumber(string armaNumber)
        {
            return double.Parse(armaNumber);
        }

        private int parseArmaNumberToInt(string armaNumber)
        {
            return (int)Math.Round(parseArmaNumber(armaNumber));
        }

        private bool isUpdateAvailable()
        {
            return false;
        } // TODO

        private bool isdigit(char ch)
        {
            int nmbr = 0;
            return int.TryParse(new string(ch, 1), out nmbr);
        }

        private bool isTrue(string ch)
        {
            return ch.ToLower() == "true";
        }

        private bool isConnected(ulong serverConnectionHandlerID)
        {
            return client.GetServersList().FirstOrDefault(x => x.ID == serverConnectionHandlerID.ToString()).ID == serverConnectionHandlerID.ToString();
        }

        private bool isTalking(ulong currentServerConnectionHandlerID, ulong myId, ulong playerId)
        {
            return talking[playerId.ToString()];
        }

        private float sq(float x) { return x * x; }

        private float distance(IrrKlang.Vector3D from, IrrKlang.Vector3D to)
        {
            return (float)Math.Sqrt(sq(from.X - to.X) + sq(from.Y - to.Y) + sq(from.Z - to.Z));
        }

        private float distanceForDiverRadio(float d, ulong serverConnectionHandlerID)
        {
            float wavesLevel = 0.0f;
            wavesLevel = serverIdToData[serverConnectionHandlerID].wavesLevel;
            return Constants.DD_MIN_DISTANCE + (Constants.DD_MAX_DISTANCE - Constants.DD_MIN_DISTANCE) * (1.0f - wavesLevel);
        }

        private float distanceFromClient(ulong serverConnectionHandlerID, CLIENT_DATA data)
        {
            IrrKlang.Vector3D myPosition = serverIdToData[serverConnectionHandlerID].myPosition;
            IrrKlang.Vector3D clientPosition = data.clientPosition;
            float d = distance(myPosition, clientPosition);
            return d;
        }

        private float effectiveDistance(ulong serverConnectionHandlerID, CLIENT_DATA data)
        {
            IrrKlang.Vector3D myPosition = serverIdToData[serverConnectionHandlerID].myPosition;
            IrrKlang.Vector3D clientPosition = data.clientPosition;
            float d = distance(myPosition, clientPosition);
            float result = d
                + (data.terrainInterception * serverIdToData[serverConnectionHandlerID].terrainIntersectionCoefficient)
                + (data.terrainInterception * serverIdToData[serverConnectionHandlerID].terrainIntersectionCoefficient * d / 2000.0f);
            result *= serverIdToData[serverConnectionHandlerID].receivingDistanceMultiplicator;
            return result;
        }

        private LISTED_INFO isOverLocalRadio(ulong serverConnectionHandlerID, CLIENT_DATA data, CLIENT_DATA myData, bool ignoreSwTangent, bool ignoreLrTangent, bool ignoreDdTangent)
        {
            LISTED_INFO result = new LISTED_INFO();
            result.over = OVER_RADIO_TYPE.LISTEN_TO_NONE;
            result.volume = 0;
            result.on = LISTED_ON.LISTED_ON_NONE;
            result.waveZ = 1.0f;
            if (data == null || myData == null) return result;

            IrrKlang.Vector3D myPosition = serverIdToData[serverConnectionHandlerID].myPosition;
            IrrKlang.Vector3D clientPosition = data.clientPosition;

            result.pos.X = result.pos.Y = result.pos.Z = 0.0f;

            result.radio_id = "local_radio";
            result.vehicle = getVehicleDescriptor(myData.vehicleId);

            float d = effectiveDistance(serverConnectionHandlerID, data);

            if ((data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_SW || ignoreSwTangent) && serverIdToData[serverConnectionHandlerID].myLrFrequencies.ContainsKey(data.frequency))
            {
                if (data.canUseSWRadio && myData.canUseLRRadio && d < data.range)
                {
                    result.over = OVER_RADIO_TYPE.LISTEN_TO_SW;
                    result.on = LISTED_ON.LISTED_ON_LR;
                    result.volume = serverIdToData[serverConnectionHandlerID].myLrFrequencies[data.frequency].volume;
                    result.stereoMode = serverIdToData[serverConnectionHandlerID].myLrFrequencies[data.frequency].stereoMode;
                }
            }

            if ((data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_SW || ignoreSwTangent) && serverIdToData[serverConnectionHandlerID].mySwFrequencies.ContainsKey(data.frequency))
            {
                if (data.canUseSWRadio && myData.canUseSWRadio && d < data.range)
                {
                    result.over = OVER_RADIO_TYPE.LISTEN_TO_SW;
                    result.on = LISTED_ON.LISTED_ON_SW;
                    result.volume = serverIdToData[serverConnectionHandlerID].mySwFrequencies[data.frequency].volume;
                    result.stereoMode = serverIdToData[serverConnectionHandlerID].mySwFrequencies[data.frequency].stereoMode;
                }
            }

            if ((data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_LR || ignoreLrTangent) && serverIdToData[serverConnectionHandlerID].mySwFrequencies.ContainsKey(data.frequency))
            {
                if (data.canUseSWRadio && myData.canUseLRRadio && d < data.range)
                {
                    result.over = OVER_RADIO_TYPE.LISTEN_TO_LR;
                    result.on = LISTED_ON.LISTED_ON_SW;
                    result.volume = serverIdToData[serverConnectionHandlerID].mySwFrequencies[data.frequency].volume;
                    result.stereoMode = serverIdToData[serverConnectionHandlerID].mySwFrequencies[data.frequency].stereoMode;
                }
            }

            if ((data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_LR || ignoreLrTangent) && serverIdToData[serverConnectionHandlerID].myLrFrequencies.ContainsKey(data.frequency))
            {
                if (data.canUseLRRadio && myData.canUseLRRadio && d < data.range)
                {
                    result.over = OVER_RADIO_TYPE.LISTEN_TO_LR;
                    result.on = LISTED_ON.LISTED_ON_LR;
                    result.volume = serverIdToData[serverConnectionHandlerID].myLrFrequencies[data.frequency].volume;
                    result.stereoMode = serverIdToData[serverConnectionHandlerID].myLrFrequencies[data.frequency].stereoMode;
                }
            }

            if ((data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_DD || ignoreSwTangent) && serverIdToData[serverConnectionHandlerID].myDdFrequency == data.frequency)
            {
                float dUnderWater = distance(myPosition, clientPosition);
                if (data.canUseDDRadio && myData.canUseDDRadio && dUnderWater < distanceForDiverRadio(dUnderWater, serverConnectionHandlerID))
                {
                    result.over = OVER_RADIO_TYPE.LISTEN_TO_DD;
                    result.on = LISTED_ON.LISTED_ON_DD;
                    result.volume = serverIdToData[serverConnectionHandlerID].ddVolumeLevel;
                    result.stereoMode = 0;
                }
            }

            return result;
        }

        private List<LISTED_INFO> isOverRadio(ulong serverConnectionHandlerID, CLIENT_DATA data, CLIENT_DATA myData, bool ignoreSwTangent, bool ignoreLrTangent, bool ignoreDdTangent)
        {
            LinkedList<LISTED_INFO> result = new LinkedList<LISTED_INFO>();
            if (data == null || myData == null) return result.ToList();
            if (data.clientId != myData.clientId)
            {
                LISTED_INFO local = isOverLocalRadio(serverConnectionHandlerID, data, myData, ignoreSwTangent, ignoreLrTangent, ignoreDdTangent);
                if ((local.on != LISTED_ON.LISTED_ON_NONE) && (local.over != OVER_RADIO_TYPE.LISTEN_TO_NONE))
                {
                    result.AddLast(local);
                }
            }

            IrrKlang.Vector3D myPosition = serverIdToData[serverConnectionHandlerID].myPosition;
            IrrKlang.Vector3D clientPosition = data.clientPosition;
            float d = effectiveDistance(serverConnectionHandlerID, data);
            string nickname = getClientNickname(serverConnectionHandlerID, data.clientId);

            if (d < data.range)
            {
                if ((data.canUseSWRadio && (data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_SW || ignoreSwTangent)) || (data.canUseLRRadio && (data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_LR || ignoreLrTangent)) || (data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_DD || ignoreDdTangent))
                {
                    foreach(var itr in serverIdToData[serverConnectionHandlerID].speakers.GetDictionary())
                    {
                        if ((itr.Key == data.frequency) && (itr.Value.nickname != nickname))
                        {
                            SPEAKER_DATA speaker = itr.Value;
                            LISTED_INFO info = new LISTED_INFO();
                            info.on = LISTED_ON.LISTED_ON_GROUND;
                            info.over = (data.tangentOverType == OVER_RADIO_TYPE.LISTEN_TO_SW || ignoreSwTangent) ? OVER_RADIO_TYPE.LISTEN_TO_SW : OVER_RADIO_TYPE.LISTEN_TO_LR;
                            info.radio_id = speaker.radio_id;
                            info.stereoMode = 0;
                            info.vehicle = speaker.vehicle;
                            info.volume = speaker.volume;
                            info.waveZ = speaker.waveZ;
                            bool posSet = false;
                            if(speaker.pos.Count == 3)
                            {
                                info.pos.X = speaker.pos.ToArray()[0];
                                info.pos.Y = speaker.pos.ToArray()[1];
                                info.pos.Z = speaker.pos.ToArray()[2];
                                posSet = true;
                            }
                            else
                            {
                                int currentDataFrame = serverIdToData[serverConnectionHandlerID].currentDataFrame;
                                if(serverIdToData[serverConnectionHandlerID].nicknameToClientData.ContainsKey(speaker.nickname))
                                {
                                    CLIENT_DATA clientData = serverIdToData[serverConnectionHandlerID].nicknameToClientData[speaker.nickname];
                                    if (Math.Abs(currentDataFrame - clientData.dataFrame) <= 1)
                                    {
                                        info.pos = clientData.clientPosition;
                                        posSet = true;
                                    }
                                }
                            }
                            if (posSet)
                                result.AddLast(info);
                        }
                    }
                }
            }

            return result.ToList();
        }

        private void Commands()
        {
            commandManager.Add(new Commands.CommandStub("TS_INFO", (e) =>
            {
                e.Pipe.Write(DiscordInfo(client, e.Parameters[0]));
            }));
            commandManager.Add(new Commands.CommandStub("KILLED", (e) =>
            {
                processUnitKilled(e.Parameters[0], e.CurrentServerConnectionHandlerID);
                e.Pipe.Write("DONE");
            }));
            commandManager.Add(new Commands.CommandStub("SPEAKERS", (e) =>
            {
                processSpeakers(e.Parameters.ToList(), e.CurrentServerConnectionHandlerID);
                e.Pipe.Write("OK");
            }));
            commandManager.Add(new Commands.CommandStub("RELEASE_ALL_TANGENTS", (e) =>
            {

            })); // TODO
            commandManager.Add(new Commands.CommandStub("VERSION", (e) =>
            {
                serverIdToData[e.CurrentServerConnectionHandlerID].addon_version = e.Parameters[0];
                serverIdToData[e.CurrentServerConnectionHandlerID].serious_mod_channel_name = e.Parameters[1];
                serverIdToData[e.CurrentServerConnectionHandlerID].serious_mod_channel_password = e.Parameters[2];
                serverIdToData[e.CurrentServerConnectionHandlerID].currentDataFrame++;

                e.Pipe.Write("OK");
            }));
            commandManager.Add(new Commands.CommandStub("TRACK", (e) => { })); // NO TRACKING
            commandManager.Add(new Commands.CommandStub("POS", (e) =>
            {
                IrrKlang.Vector3D position = new IrrKlang.Vector3D();
                position.X = float.Parse(e.Parameters[1]);
                position.Y = float.Parse(e.Parameters[2]);
                position.Z = float.Parse(e.Parameters[3]);
                e.Pipe.Write(processUnitPosition(e.Parameters[0], e.CurrentServerConnectionHandlerID, position, float.Parse(e.Parameters[4]),
                    isTrue(e.Parameters[5]), isTrue(e.Parameters[6]), isTrue(e.Parameters[7]), isTrue(e.Parameters[8]), e.Parameters[9], int.Parse(e.Parameters[10]), float.Parse(e.Parameters[11]), float.Parse(e.Parameters[12])));
            }));
            commandManager.Add(new Commands.CommandStub("TANGENT", (e) =>
            {
                bool pressed = (e.Parameters[0] == "PRESSED");
                bool longRange = (e.UsedCommand == "TANGENT_LR");
                bool diverRadio = (e.UsedCommand == "TANGENT_DD");
                string subType = e.Parameters[3];

                bool changed = false;
                if (serverIdToData.ContainsKey(e.CurrentServerConnectionHandlerID))
                {
                    changed = (serverIdToData[e.CurrentServerConnectionHandlerID].tangentPressed != pressed);
                    serverIdToData[e.CurrentServerConnectionHandlerID].tangentPressed = pressed;
                    if(serverIdToData[e.CurrentServerConnectionHandlerID].nicknameToClientData.ContainsKey(serverIdToData[e.CurrentServerConnectionHandlerID].myNickname))
                    {
                        CLIENT_DATA clientData = serverIdToData[e.CurrentServerConnectionHandlerID].nicknameToClientData[serverIdToData[e.CurrentServerConnectionHandlerID].myNickname];
                        if (longRange) clientData.canUseLRRadio = true;
                        else if (diverRadio) clientData.canUseDDRadio = true;
                        else clientData.canUseSWRadio = true;
                        clientData.subtype = subType;
                    }
                }

                if (changed)
                {
                    string commandToBroadcast = e.RawCommand + "\t" + serverIdToData[e.CurrentServerConnectionHandlerID].myNickname;
                    if(pressed)
                    {
                        if (subType == "digital_lr") Play2DAudio("radio-sounds/lr/local_start.wav");
                        else if (subType == "dd") Play2DAudio("radio-sounds/dd/local_start.wav");
                        else if (subType == "digital") Play2DAudio("radio-sounds/sw/local_start.wav");
                        else if (subType == "airborne") Play2DAudio("radio-sounds/ab/local_start.wav");
                        if (!waiting_tangent_off)
                        {
                            vadEnabled = hlp_checkVad();
                            if (vadEnabled) hlp_disableVad();
                            disableVoiceAndSendCommand(commandToBroadcast, e.CurrentServerConnectionHandlerID, pressed);
                        }
                        else skip_tangent_off = true;
                    }
                    else
                    {
                        PTTDelayArguments args = new PTTDelayArguments();
                        args.commandToBroadcast = commandToBroadcast;
                        args.currentServerConnectionHandlerID = e.CurrentServerConnectionHandlerID;
                        args.subtype = subType;
                        ptt_arguments = args;

                        if (ptt_arguments.subtype == "digital_lr") Play2DAudio("radio-sounds/lr/local_end.wav");
                        else if (ptt_arguments.subtype == "dd") Play2DAudio("radio-sounds/dd/local_end.wav");
                        else if (ptt_arguments.subtype == "digital") Play2DAudio("radio-sounds/sw/local_end.wav");
                        else if (ptt_arguments.subtype == "airborne") Play2DAudio("radio-sounds/ab/local_end.wav");
                    }
                }

                e.Pipe.Write("OK");
            }).AddAlias("TANGENT_LR").AddAlias("TANGENT_DD")); // TODO
            commandManager.Add(new Commands.CommandStub("FREQ", (e) =>
            {

            })); // TODO
            commandManager.Add(new Commands.CommandStub("IS_SPEAKING", (e) =>
            {

                e.Pipe.Write("NOT_SPEAKING");
            })); // TODO
        }

        private bool hlp_checkVad()
        {
            return true;
        } // TODO

        private void hlp_enableVad()
        {

        } // TODO

        private void hlp_disableVad()
        {

        } // TODO

        private void disableVoiceAndSendCommand(string commandToBroadcast, ulong currentServerConnectionHandlerID, bool pressed)
        {

        } // TODO

        private string DiscordInfo(DiscordClient client, string command)
        {
            if (command == "SERVER")
            {
                if (client.GetVoiceClient().Channel == null)
                {
                    return "ERROR_GETTING_SERVER_NAME";
                }
                else
                {
                    return client.GetVoiceClient().Channel.Parent.Name;
                }
            }
            else if (command == "CHANNEL")
            {
                if (client.GetVoiceClient().Channel != null)
                    return client.GetVoiceClient().Channel.Name;
                else
                    return client.GetServersList()[0].Channels[0].Name;
            }
            else if (command == "PING")
            {
                return "PONG";
            }
            return "FAIL";
        }

        private void processSpeakers(List<string> tokens, ulong currentServerConnectionHandlerID)
        {
            serverIdToData[currentServerConnectionHandlerID].speakers.Clear();
            if (tokens.Count == 2)
            {
                string[] speakers = tokens[0].Split(0xB.ToString().ToCharArray());
                for(int q = 0; q < speakers.Length; q++)
                {
                    if (speakers[q].Length > 0)
                    {
                        SPEAKER_DATA data = new SPEAKER_DATA();
                        string[] parts = speakers[q].Split(0xA.ToString().ToCharArray());
                        data.radio_id = parts[0];
                        string[] freqs = parts[1].Split(new char[] { '|' });
                        data.nickname = parts[2];
                        string coordinates = parts[3];
                        if (coordinates.Length > 2)
                        {
                            string[] c = coordinates.Substring(1, coordinates.Length - 2).Split(new char[] { ',' });
                            for (int p = 0; p < 3; p++)
                                data.pos.AddLast(float.Parse(c[p]));
                        }
                        data.volume = parseArmaNumberToInt(parts[4]);
                        data.vehicle = getVehicleDescriptor(parts[5]);
                        if (parts.Length > 6)
                            data.waveZ = float.Parse(parts[6]);
                        else
                            data.waveZ = 1;
                        for (int p = 0; p < freqs.Length; p++)
                            serverIdToData[currentServerConnectionHandlerID].speakers.Add(freqs[p], data);
                    }
                }
            }
        }

        private KeyValuePair<string, float> getVehicleDescriptor(string vehicleId)
        {
            string first = "";
            float second = 0.0f;

            if (vehicleId.IndexOf("_turnout") != -1)
                first = vehicleId.Substring(0, vehicleId.IndexOf("_turnout"));
            else
            {
                if (vehicleId.LastIndexOf("_") != -1)
                {
                    first = vehicleId.Substring(0, vehicleId.LastIndexOf("_"));
                    second = float.Parse(vehicleId.Substring(vehicleId.LastIndexOf("_") + 1));
                }
                else
                {
                    first = vehicleId;
                }
            }

            return new KeyValuePair<string, float>(first, second);
        }

        private void processUnitKilled(string name, ulong serverConnection)
        {
            if(serverIdToData.ContainsKey(serverConnection))
            {
                if(serverIdToData[serverConnection].nicknameToClientData.ContainsKey(name))
                {
                    CLIENT_DATA clientData = serverIdToData[serverConnection].nicknameToClientData[name];
                    if(clientData != null)
                    {
                        clientData.dataFrame = Constants.INVALID_DATA_FRAME;
                    }
                }
            }
            setMuteForDeadPlayers(serverConnection, isSeriousModeEnabled(serverConnection, getMyID(serverConnection)));
        }

        private bool isSeriousModeEnabled(ulong serverConnectionHandlerID, ulong clientId)
        {
            string serious_mod_channel_name = "__unknown__";
            if (serverIdToData.ContainsKey(serverConnectionHandlerID))
            {
                serious_mod_channel_name = serverIdToData[serverConnectionHandlerID].serious_mod_channel_name;
            }
            return (serious_mod_channel_name != "") && isInChannel(serverConnectionHandlerID, clientId, serious_mod_channel_name);
        }

        private bool isInChannel(ulong serverConnectionHandlerID, ulong clientId, string channelToCheck)
        {
            return getChannelName(serverConnectionHandlerID, clientId) == channelToCheck;
        }

        private string getChannelName(ulong serverConnectionHandlerID, ulong clientId)
        {
            if (clientId == ulong.MaxValue - 1) return "";
            string channelName = "";

            if ((channelName = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.First(x => x.ID == clientId.ToString()).CurrentVoiceChannel.Name) == "")
            {
                return "";
            }

            return channelName;
        }

        private void setMuteForDeadPlayers(ulong serverConnectionHandlerID, bool isSeriousModeEnabled)
        {
            bool alive = false;
            alive = serverIdToData[serverConnectionHandlerID].alive;
            LinkedList<ulong> clientsIds = getChannelClients(serverConnectionHandlerID, getCurrentChannel(serverConnectionHandlerID));
            ulong myId = getMyID(serverConnectionHandlerID);
            for (var it = clientsIds.First; it != clientsIds.Last; it = it.Next)
            {
                if (!(it.Value == myId))
                {
                    if (!hasClientData(serverConnectionHandlerID, it.Value))
                    {
                        setClientMuteStatus(serverConnectionHandlerID, it.Value, alive && isSeriousModeEnabled);
                    }
                }
            }
        }

        private bool hasClientData(ulong serverConnectionHandlerID, ulong clientID)
        {
            bool result = false;
            IntPtr time = new IntPtr(Environment.TickCount);
            int currentDataFrame = serverIdToData[serverConnectionHandlerID].currentDataFrame;
            for(int it = 0; it < serverIdToData[serverConnectionHandlerID].nicknameToClientData.Count; it++)
            {
                var clDat = serverIdToData[serverConnectionHandlerID].nicknameToClientData.Values.ToArray()[it];
                if (clDat.clientId == clientID && (Math.Abs(currentDataFrame - clDat.dataFrame) <= 1))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private CLIENT_DATA getClientData(ulong serverConnectionHandlerID, ulong clientID)
        {
            CLIENT_DATA data = null;
            foreach(KeyValuePair<string, CLIENT_DATA> it in serverIdToData[serverConnectionHandlerID].nicknameToClientData)
            {
                if (it.Value.clientId == clientID)
                {
                    data = it.Value;
                    break;
                }
            }
            return data;
        }

        private void setClientMuteStatus(ulong serverConnectionHandlerID, ulong clientId, bool status)
        {
            ulong[] clientIds = new ulong[2];
            clientIds[0] = clientId;
            clientIds[0] = 0;
            if (clientIds[0] <= 0)
                return;

            //client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members[clientId.ToString()].Muted = status;
        } // TODO

        private LinkedList<ulong> getChannelClients(ulong serverConnectionHandlerID, ulong channelId)
        {
            LinkedList<ulong> result = new LinkedList<ulong>();
            List<DSharpPlus.Objects.DiscordMember> clients = new List<DSharpPlus.Objects.DiscordMember>();
            if((clients = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.Where(x => x.CurrentVoiceChannel.ID == channelId.ToString()).ToList()).Count > 0)
            {
                int i = 0;
                while(clients[i] != null)
                {
                    result.AddLast(ulong.Parse(clients[i].ID));
                    i++;
                }
            }
            return result;
        }

        private ulong getMyID(ulong serverConnectionHandlerID)
        {
            ulong myID = ulong.MaxValue - 1;
            if(client != null && client.WebsocketAlive)
            {
                myID = ulong.Parse(client.Me.ID);
            }
            return myID;
        }

        private string getMyNickname(ulong serverConnectionHandlerID)
        {
            string bufferForNickname = "";
            ulong myId = getMyID(serverConnectionHandlerID);
            if (myId == ulong.MaxValue - 1) return "";
            if ((bufferForNickname = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.First(x => x.ID == myId.ToString()).Nickname) == "")
            {
                bufferForNickname = client.Me.Username;
            }
            string result = bufferForNickname;
            return result;
        }

        private ulong getCurrentChannel(ulong serverConnectionHandlerID)
        {
            DSharpPlus.Objects.DiscordChannel channel;
            if((channel = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.First(x => x.ID == getMyID(serverConnectionHandlerID).ToString()).CurrentVoiceChannel) == null)
            {
                channel = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Channels.First(x => x.Type == DSharpPlus.Objects.ChannelType.Voice);
            }
            return ulong.Parse(channel.ID);
        }

        private ulong getClientId(ulong serverConnectionHandlerID, string nickname)
        {
            ulong clientId = 0;
            if (serverIdToData.ContainsKey(serverConnectionHandlerID) && serverIdToData[serverConnectionHandlerID].nicknameToClientData.ContainsKey(nickname))
            {
                clientId = serverIdToData[serverConnectionHandlerID].nicknameToClientData[nickname].clientId;
            }
            return clientId;
        }

        private string getClientNickname(ulong serverConnectionHandlerID, ulong clientID)
        {
            string nickname = "Unknown nickname";
            foreach(var it in serverIdToData[serverConnectionHandlerID].nicknameToClientData)
            {
                if (it.Value.clientId == clientID)
                {
                    nickname = it.Key;
                    break;
                }
            }
            return nickname;
        }

        private string processUnitPosition(string nickname, ulong serverConnection, IrrKlang.Vector3D position, float viewAngle, bool canSpeak,
            bool canUseSWRadio, bool canUseLRRadio, bool canUseDDRadio, string vehicleID, int terrainInterception, float voiceVolume, float currentUnitDirection)
        {
            ulong time = ulong.Parse(Environment.TickCount.ToString());
            ulong myId = ulong.Parse(client.Me.ID);
            ulong playerId = 0;
            bool clientTalkingOnRadio = false;

            if(serverIdToData.ContainsKey(serverConnection))
            {
                IrrKlang.Vector3D zero = new IrrKlang.Vector3D();
                zero.X = zero.Y = zero.Z = 0.0f;
                if (nickname == serverIdToData[serverConnection].myNickname)
                {
                    CLIENT_DATA clientData = null;
                    if (serverIdToData[serverConnection].nicknameToClientData.ContainsKey(nickname))
                        clientData = serverIdToData[serverConnection].nicknameToClientData[nickname];

                    if (clientData != null)
                    {
                        playerId = myId;
                        clientData.clientId = myId;
                        clientData.clientPosition = position;
                        clientData.positionTime = time;
                        clientData.canSpeak = canSpeak;
                        clientData.canUseSWRadio = canUseSWRadio;
                        clientData.canUseLRRadio = canUseLRRadio;
                        clientData.canUseDDRadio = canUseDDRadio;
                        clientData.vehicleId = vehicleID;
                        clientData.terrainInterception = terrainInterception;
                        clientData.dataFrame = serverIdToData[serverConnection].currentDataFrame;
                        clientData.viewAngle = viewAngle;
                        clientData.voiceVolumeMultiplier = voiceVolume;
                        clientTalkingOnRadio = (clientData.tangentOverType != OVER_RADIO_TYPE.LISTEN_TO_NONE) || clientData.clientTalkingNow;
                    }
                    serverIdToData[serverConnection].myPosition = position;
                    serverIdToData[serverConnection].canSpeak = canSpeak;
                    // TODO -> 3D Listener Attributes
                }
                else
                {
                    if (!serverIdToData[serverConnection].nicknameToClientData.ContainsKey(nickname))
                    {
                        if (isConnected(serverConnection)) updateNicknameList(serverConnection);
                    }
                    if (serverIdToData[serverConnection].nicknameToClientData.ContainsKey(nickname))
                    {
                        CLIENT_DATA clientData = serverIdToData[serverConnection].nicknameToClientData[nickname];
                        if (clientData != null)
                        {
                            playerId = clientData.clientId;
                            clientData.clientPosition = position;
                            clientData.positionTime = time;
                            clientData.canSpeak = canSpeak;
                            clientData.canUseSWRadio = canUseSWRadio;
                            clientData.canUseLRRadio = canUseLRRadio;
                            clientData.canUseDDRadio = canUseDDRadio;
                            clientData.vehicleId = vehicleID;
                            clientData.terrainInterception = terrainInterception;
                            clientData.dataFrame = serverIdToData[serverConnection].currentDataFrame;
                            clientData.voiceVolumeMultiplier = voiceVolume;
                            clientTalkingOnRadio = (clientData.tangentOverType != OVER_RADIO_TYPE.LISTEN_TO_NONE) || clientData.clientTalkingNow;
                        }
                    }
                    if (serverIdToData[serverConnection].nicknameToClientData.ContainsKey(serverIdToData[serverConnection].myNickname))
                    {
                        CLIENT_DATA myData = serverIdToData[serverConnection].nicknameToClientData[serverIdToData[serverConnection].myNickname];
                        myData.viewAngle = currentUnitDirection;
                    }
                    if(isConnected(serverConnection))
                    {
                        setGameClientMuteStatus(serverConnection, getClientId(serverConnection, nickname));

                        // TODO -> 3D Sound
                    }
                }
            }

            if (playerId != 0)
            {
                if (isTalking(serverConnection, myId, playerId) || clientTalkingOnRadio)
                    return "SPEAKING";
            }
            return "NOT_SPEAKING";
        } // TODO

        private void setGameClientMuteStatus(ulong serverConnectionHandlerID, ulong clientId)
        {
            bool mute = false;
            if(isSeriousModeEnabled(serverConnectionHandlerID, clientId))
            {
                CLIENT_DATA data = hasClientData(serverConnectionHandlerID, clientId) ? getClientData(serverConnectionHandlerID, clientId) : null;
                CLIENT_DATA myData = hasClientData(serverConnectionHandlerID, getMyID(serverConnectionHandlerID)) ? getClientData(serverConnectionHandlerID, getMyID(serverConnectionHandlerID)) : null;
                bool alive = false;
                if (data != null && myData != null && serverIdToData[serverConnectionHandlerID].alive)
                {
                    List<LISTED_INFO> listedInfo = isOverRadio(serverConnectionHandlerID, data, myData, false, false, false);
                    if (listedInfo.Count == 0)
                    {
                        bool isTalk = data.clientTalkingNow || isTalking(serverConnectionHandlerID, getMyID(serverConnectionHandlerID), clientId);
                        mute = (distanceFromClient(serverConnectionHandlerID, data) > data.voiceVolume) || (!isTalk);
                    }
                    else
                    {
                        mute = false;
                    }
                }
                else
                {
                    mute = true;
                }
                if (mute) data.resetVoices();
            }
            setClientMuteStatus(serverConnectionHandlerID, clientId, mute);
        }

        private void updateNicknameList(ulong serverConnectionHandlerID)
        {
            LinkedList<ulong> clients = getChannelClients(serverConnectionHandlerID, getCurrentChannel(serverConnectionHandlerID));

            for(var clientIdIterator = clients.First; clientIdIterator != clients.Last; clientIdIterator = clientIdIterator.Next)
            {
                ulong clientId = clientIdIterator.Value;
                string name;
                if ((name = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.First(x => x.ID == clientId.ToString()).Nickname) == "")
                {
                    name = client.GetServersList().First(x => x.ID == serverConnectionHandlerID.ToString()).Members.Values.First(x => x.ID == clientId.ToString()).Username;
                }
                else
                {
                    if (serverIdToData.ContainsKey(serverConnectionHandlerID))
                    {
                        string clientNickname = name;
                        if (!serverIdToData[serverConnectionHandlerID].nicknameToClientData.ContainsKey(clientNickname))
                        {
                            serverIdToData[serverConnectionHandlerID].nicknameToClientData[clientNickname] = new CLIENT_DATA();
                        }
                        CLIENT_DATA data = serverIdToData[serverConnectionHandlerID].nicknameToClientData[clientNickname];
                        data.clientId = clientId;
                    }
                }
            }
            string myNickname = getMyNickname(serverConnectionHandlerID);
            serverIdToData[serverConnectionHandlerID].myNickname = myNickname;
        }
    }
}
