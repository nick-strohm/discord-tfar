﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Interfaces
{
    public interface IPlugin
    {
        char[] Plugin_Name { get; set; }
        char[] Plugin_Version { get; set; }
        char[] Lib_Version { get; set; }
        char[] Plugin_Author { get; set; }
        char[] Plugin_Description { get; set; }
        void Plugin_SetFunctionPointers(object funcs);
        int Plugin_Init();
        void Plugin_Shutdown();

        int Plugin_OffersConfigure();
        void Plugin_RegisterPluginID(char[] id);
        char[] Plugin_CommandKeyword { get; set; }
        int Plugin_ProcessCommand(ulong serverConnectionHandlerID, char[] command);
        char[] Plugin_InfoTitle { get; set; }
        //void Plugin_InfoData(ulong serverConnectionHandlerID, ulong id, PluginItemType type, char[][] data);
        void Plugin_FreeMemory(object[] data);
        int Plugin_RequestAutoload();

        void Plugin_OnConnectStatusChangeEvent(ulong serverConnectionHandlerID, int newStatus, uint errorNumber);
    }
}
