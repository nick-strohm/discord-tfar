﻿using Discord_TFAR.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Interfaces
{
    public interface ICommand
    {
        string CommandName { get; set; }
        Action<CommandArgs> CommandFunc { get; set; }

        void Trigger(Tools.Pipe pipe, string command, string parameters);
        void Trigger(Tools.Pipe pipe, string command, string[] parameters);
    }
}
