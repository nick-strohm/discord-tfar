﻿using DSharpPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class Pipe
    {
        string networkHost = ".";
        string pipeName = "task_force_radio_pipe";
        NamedPipeClientStream pipeClient;
        StreamReader streamr;
        StreamWriter streamw;
        System.Threading.Thread t;

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        protected virtual void OnMessageReceived(object sender, MessageReceivedEventArgs e)
        {
            MessageReceived?.Invoke(sender, e);
        }

        public Pipe()
        {
            pipeClient = new NamedPipeClientStream(networkHost, pipeName, PipeDirection.InOut, PipeOptions.Asynchronous, System.Security.Principal.TokenImpersonationLevel.Impersonation);
            t = new System.Threading.Thread(Listen);
            
            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Constructed", TimeStamp = DateTime.Now }, "PIPE");

        }
        
        private void Listen()
        {
            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Listening", TimeStamp = DateTime.Now }, "PIPE");
            while (pipeClient.IsConnected)
            {
                try
                {
                    string message = streamr.ReadLine();
                    WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Received:{Environment.NewLine}    {message}", TimeStamp = DateTime.Now }, "PIPE");

                    OnMessageReceived(this, new MessageReceivedEventArgs() { Message = message });

                    Write("OK");
                }
                catch
                {

                }
            }
        }

        public void Start()
        {
            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Starting", TimeStamp = DateTime.Now }, "PIPE");
            pipeClient.Connect();
            if (pipeClient.IsConnected)
            {
                streamw = new StreamWriter(pipeClient);
                streamr = new StreamReader(pipeClient);
                t.Start();
            }
            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Started", TimeStamp = DateTime.Now }, "PIPE");
        }

        public void Stop()
        {
            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Stopping", TimeStamp = DateTime.Now }, "PIPE");
            pipeClient.Close();
        }

        public void Write(string message)
        {
            char[] output = message.ToCharArray();

            WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Sending: {output.ToString()} (Test: {output})", TimeStamp = DateTime.Now }, "PIPE");

            streamw.Write(output);
            streamw.Flush();
        }

        public static void WriteDebug(LogMessage m, string prefix)
        {
            if (!Directory.Exists("log")) Directory.CreateDirectory("log");
            if (!File.Exists($"log/{m.TimeStamp.Year}-{m.TimeStamp.Month}-{m.TimeStamp.Day}.log"))
            {
                var file = File.Create($"log/{m.TimeStamp.Year}-{m.TimeStamp.Month}-{m.TimeStamp.Day}.log");
                file.Close();
            }
            File.AppendAllText($"log/{m.TimeStamp.Year}-{m.TimeStamp.Month}-{m.TimeStamp.Day}.log", $"[{m.Level} | {prefix.ToUpper()}] {m.Message}{Environment.NewLine}");

            if (m.Level == MessageLevel.Unecessary)
                return;
            switch (m.Level)
            {
                case MessageLevel.Debug:
                    System.Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case MessageLevel.Error:
                    System.Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Critical:
                    System.Console.ForegroundColor = ConsoleColor.White;
                    System.Console.BackgroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Warning:
                    System.Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
            System.Console.Write($"[{prefix}: {m.TimeStamp}:{m.TimeStamp.Millisecond}]: ");
            System.Console.ForegroundColor = ConsoleColor.White;
            System.Console.Write(m.Message + "\n");
            System.Console.BackgroundColor = ConsoleColor.Black;
        }

        public bool IsConnected() => pipeClient.IsConnected;
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public string Message;
    }
}
