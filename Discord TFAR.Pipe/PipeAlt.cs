﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Tools
{
    public class PipeAlt
    {
        static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);
        static readonly IntPtr PIPE_TYPE_BYTE = new IntPtr(0x00000000);
        static readonly IntPtr PIPE_TYPE_MESSAGE = new IntPtr(0x00000004);
        static readonly IntPtr PIPE_READMODE_BYTE = new IntPtr(0x00000000);
        static readonly IntPtr PIPE_READMODE_MESSAGE = new IntPtr(0x00000002);
        static readonly IntPtr PIPE_WAIT = new IntPtr(0x00000000);
        static readonly IntPtr PIPE_NOWAIT = new IntPtr(0x00000001);
        static readonly IntPtr PIPE_ACCEPT_REMOTE_CLIENTS = new IntPtr(0x00000000);
        static readonly IntPtr PIPE_REJECT_REMOTE_CLIENTS = new IntPtr(0x00000008);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct CRITICAL_SECTION
        {
            IntPtr DebugInfo;
            long LockCount;
            long RecursionCount;
            IntPtr OwningThread;
            IntPtr LockSemaphore;
            ulong SpinCount;
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr CreateFile(
            [MarshalAs(UnmanagedType.LPTStr)] string filename,
            [MarshalAs(UnmanagedType.U4)] FileAccess access,
            [MarshalAs(UnmanagedType.U4)] FileShare share, 
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes flagsAndAttributes,
            IntPtr templateFile
            );

        [DllImport("kernel32.dll")]
        static extern bool SetNamedPipeHandleState(
            IntPtr hNamedPipe,
            IntPtr lpMode,
            IntPtr lpMaxCollectionCount,
            IntPtr lpCollectDataTimeout
            );

        [DllImport("kernel32.dll")]
        static extern uint GetLastError();

        [DllImport("kernel32.dll", EntryPoint = "PeekNamedPipe", SetLastError = true)]
        static extern bool PeekNamedPipe(
            IntPtr handle,
            byte[] buffer,
            uint nBufferSize,
            ref uint bytesRead,
            ref uint bytesAvail,
            ref uint BytesLeftThisMessage
            );

        [DllImport("msvcrt.dll", EntryPoint = "memset", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        public static extern IntPtr MemSet(
            IntPtr dest, 
            int c, 
            int byteCount
            );

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadFile(
            IntPtr hFile, 
            [Out] byte[] lpBuffer,
            uint nNumberOfBytesToRead, 
            out uint lpNumberOfBytesRead, 
            IntPtr lpOverlapped
            );

        [DllImport("kernel32.dll")]
        static extern int InterlockedExchange(
            ref IntPtr Target,
            IntPtr Value
            );

        [DllImport("kernel32.dll")]
        static extern void EnterCriticalSection(
            ref CRITICAL_SECTION lpCriticalSection
            );

        public string pipeName = "\\\\.\\pipe\\task_force_radio_pipe";
        public bool exitThread = false;
        public IntPtr lastInGame = GetTickCount();
        CRITICAL_SECTION serverDataCriticalSection;

        static IntPtr GetTickCount()
        {
            return new IntPtr(0);
        }

        IntPtr openPipe()
        {
            IntPtr pipe = CreateFile(pipeName, FileAccess.ReadWrite, FileShare.ReadWrite, IntPtr.Zero, FileMode.OpenOrCreate, FileAttributes.Normal, IntPtr.Zero);
            uint error = GetLastError();
            IntPtr dwMode = PIPE_READMODE_MESSAGE;
            SetNamedPipeHandleState(pipe, dwMode, IntPtr.Zero, IntPtr.Zero);
            return pipe;
        }

        public IntPtr PipeThread(byte[] lpParam)
        {
            IntPtr pipe = INVALID_HANDLE_VALUE;

            IntPtr sleep = new IntPtr(50);
            while(!exitThread)
            {
                if (pipe == INVALID_HANDLE_VALUE) pipe = openPipe();

                uint numBytesRead = 0;
                uint numBytesAvail = 0;
                uint numBytesLeft = 0;
                if (PeekNamedPipe(pipe, null, 0, ref numBytesRead, ref numBytesAvail, ref numBytesLeft))
                {
                    if (numBytesAvail > 0)
                    {
                        byte[] buffer = new byte[4096];

                        bool result = ReadFile(
                            pipe,
                            buffer,
                            4096,
                            out numBytesRead,
                            IntPtr.Zero
                            );
                        if (result)
                        {
                            sleep = new IntPtr(50);
                            string command = Encoding.ASCII.GetString(buffer);
                            Console.WriteLine($"[PIPE] {command}");

                            if (!command.StartsWith("VERSION"))
                            {
                                InterlockedExchange(ref lastInGame, GetTickCount());
                                EnterCriticalSection(ref serverDataCriticalSection);
                            }
                        }
                    }
                }
            }

            pipe = INVALID_HANDLE_VALUE;
            return new IntPtr(0);
        }
    }
}
