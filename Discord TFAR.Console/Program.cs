﻿using DSharpPlus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_TFAR.Console
{
    class Program
    {
        string welcomeMsg = @"@@@@@@@  @@@@@@@@   @@@@@@   @@@@@@@   
@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@  
  @@!    @@!       @@!  @@@  @@!  @@@  
  !@!    !@!       !@!  @!@  !@!  @!@  
  @!!    @!!!:!    @!@!@!@!  @!@!!@!   
  !!!    !!!!!:    !!!@!!!!  !!@!@!    
  !!:    !!:       !!:  !!!  !!: :!!   
  :!:    :!:       :!:  !:!  :!:  !:!  
   ::     ::       ::   :::  ::   :::  
   :      :         :   : :   :   : :  ";

        static void Main(string[] args) => new Program().Run();

        public DiscordClient client;
        public Tools.Plugin plugin;

        public int[] consoleSize = new int[2];

        public void Run()
        {
            System.Console.SetWindowSize(150, 50);

            consoleSize[0] = System.Console.WindowHeight;
            consoleSize[1] = System.Console.WindowWidth;

            System.Console.ForegroundColor = ConsoleColor.Green;
            System.Console.BackgroundColor = ConsoleColor.Black;
            Tools.ConsoleTools.PrintToCenter(welcomeMsg);
            System.Console.ForegroundColor = ConsoleColor.White;
            System.Console.BackgroundColor = ConsoleColor.Black;

            Login();

            client.Connected += (sender, e) => Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Connected as {e.User.Username}#{e.User.Discriminator} on {client.CurrentGatewayURL} (v{client.DiscordGatewayVersion})", TimeStamp = DateTime.Now }, "DISCORD");
            client.GuildAvailable += (sender, e) => Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"Guild {e.Server.Name} is available", TimeStamp = DateTime.Now }, "DISCORD");
            client.SocketClosed += (sender, e) =>
            {
                if (!e.WasClean)
                {
                    client.Connect();
                    Tools.ConsoleTools.DrawConsole(plugin);
                }
            };

            plugin = new Tools.Plugin(ref client);

            Tools.ConsoleTools.DrawConsole(plugin);
            plugin.Start();

            Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Critical, Message = "Use help", TimeStamp = DateTime.Now }, "SYSTEM");

            while (true)
            {
                if (consoleSize[0] < 10)
                    System.Console.SetWindowSize(System.Console.WindowWidth, 10);
                if (consoleSize[1] < 70)
                    System.Console.SetWindowSize(70, System.Console.WindowHeight);
                if (consoleSize[0] != System.Console.WindowHeight || consoleSize[1] != System.Console.WindowWidth)
                {
                    consoleSize[0] = System.Console.WindowHeight;
                    consoleSize[1] = System.Console.WindowWidth;

                    Tools.ConsoleTools.DrawConsole(plugin);
                }

                string input = System.Console.ReadLine();
                string[] parts = input.Split(new char[] { ' ' });
                if (parts[0] == "help")
                {
                    Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = "Usable commands: help, joinvoice <channelid>, syscheck, quit", TimeStamp = DateTime.Now }, "SYSTEM");
                }
                if (parts[0] == "joinvoice")
                {
                    try
                    {
                        long channelId = long.Parse(parts[1]);
                        DSharpPlus.Objects.DiscordChannel channel = client.GetChannelByID(channelId);

                        Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Debug, Message = $"{channelId}: {channel.Name} [{channel.Type.ToString()}]", TimeStamp = DateTime.Now }, "DISCORD");

                        client.ConnectToVoiceChannel(channel, new DiscordVoiceConfig()
                        {
                            Channels = 2,
                            OpusMode = DSharpPlus.Voice.OpusApplication.MusicOrMixed,
                            SendOnly = false
                        }, true, false);
                        Tools.ConsoleTools.DrawVoiceBar(plugin);
                    } catch(Exception ex)
                    {
                        Tools.ConsoleTools.WriteDebug(new LogMessage() { Level = MessageLevel.Critical, Message = $"{ex.Message} ({ex.Source}):{Environment.NewLine}{ex.StackTrace}", TimeStamp = DateTime.Now }, "EXCEPTION");
                    }
                }
                if (parts[0] == "syscheck")
                {
                    plugin.SoundCheck();
                }
                if (parts[0] == "quit")
                {
                    plugin.Stop();
                }

                Tools.ConsoleTools.DrawInput();
            }
        }

        public void Login()
        {
            if (Tools.LoginTools.DoesUserFileExists())
            {
                System.Console.WriteLine($"{Environment.NewLine}UserFile found (Enter your salt or \"relogin\"):");
                string input = System.Console.ReadLine();
                if (input == "relogin")
                {
                    File.Delete("user.dat");
                    Login();
                } else
                {
                    try
                    {
                        Tools.LoginTools.CanLogin(input);
                        System.Console.WriteLine($"Salt is correct. Logging in...");

                        client = new DiscordClient(Tools.LoginTools.GetToken(input), false, true);
                    } catch {
                        LogMessage message = new LogMessage();
                        message.Level = MessageLevel.Warning;
                        message.Message = "Your salt or your credentials may are wrong";
                        message.TimeStamp = DateTime.Now;

                        Tools.ConsoleTools.WriteDebug(message, "AUTH");
                        Login();
                    }
                }
            }
            else
            {
                System.Console.WriteLine($"{Environment.NewLine}UserFile not found!");
                System.Console.WriteLine($"Please enter your email:");
                string email = System.Console.ReadLine();
                System.Console.WriteLine($"Please enter your password (Warning: PLAINTEXT):");
                string password = System.Console.ReadLine();
                System.Console.WriteLine($"Please enter a salt (To encrypt your credentials):");
                string salt = System.Console.ReadLine();

                System.Console.WriteLine($"{Environment.NewLine}Testing your credentials now...");
                try
                {
                    Tools.LoginTools.Save(email, password, salt);
                    System.Console.WriteLine($"Credentials tested. Logging in...");

                    client = new DiscordClient(Tools.LoginTools.GetToken(salt), false, true);
                } catch {
                    LogMessage message = new LogMessage();
                    message.Level = MessageLevel.Error;
                    message.Message = "Your credentials may are wrong";
                    message.TimeStamp = DateTime.Now;

                    Tools.ConsoleTools.WriteDebug(message, "AUTH");
                    Login();
                }
            }
        }

    }
}
