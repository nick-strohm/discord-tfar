﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discord_TFAR
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Tools.LoginTools.Save(textBox1.Text, textBox2.Text, textBox3.Text))
            {
                Close();
            }
        }

        public string getSalt() { return textBox3.Text; }
    }
}
