﻿using Discord_TFAR.Tools;
using DSharpPlus;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discord_TFAR
{
    public partial class Form1 : Form
    {
        private string token;
        private string game;
        private string username;
        private string serverId;
        private string channelId;
        private DiscordClient client;
        private ImageList images;
        private AudioTools audioTools;
        Pipe pipe = new Pipe();

        private List<Tools.ClientData> players = new List<Tools.ClientData>();

        public Form1()
        {
            InitializeComponent();

            if (Tools.LoginTools.DoesUserFileExists())
            {
                LoginForm form = new LoginForm();
                form.ShowDialog();

                if (form.Relogin)
                {
                    RegisterForm rForm = new RegisterForm();
                    rForm.ShowDialog();

                    token = getToken(rForm.getSalt());
                } else
                {
                    token = getToken(form.getSalt());
                }
            }
            else
            {
                RegisterForm form = new RegisterForm();
                form.ShowDialog();

                token = getToken(form.getSalt());
            }

            Console.WriteLine(token);
        }

        private string getToken(string salt) { return Tools.LoginTools.GetToken(salt); }

        private void Form1_Load(object sender, EventArgs e)
        {
            images = new ImageList();
            images.Images.Add("server", Image.FromFile("images/Server.png"));
            images.Images.Add("channel", Image.FromFile("images/Channel.png"));
            images.Images.Add("player", Image.FromFile("images/Player.png"));
            images.Images.Add("player_on", Image.FromFile("images/Player_On.png"));

            treeView1.ImageList = images;

            client = new DiscordClient(token, false, true);
            client.Connected += Client_Connected;
            client.GuildAvailable += Client_GuildAvailable;
            client.UserLeftVoiceChannel += Client_UserLeftVoiceChannel;
            client.VoiceStateUpdate += Client_VoiceStateUpdate;
            client.UserSpeaking += Client_UserSpeaking;
            client.MentionReceived += Client_MentionReceived;

            client.EnableVerboseLogging = true;
            client.GetTextClientLogger.LogMessageReceived += (tsender, te) => WriteDebug(te.message, "Discord");
            client.SendLoginRequest();
            client.Connect();

            audioTools = new Tools.AudioTools();

            ListenToPipe();

            Form2 form = new Form2();
            form.Show();
        }

        static void WriteDebug(LogMessage m, string prefix)
        {
            if (m.Level == MessageLevel.Unecessary)
                return;
            switch (m.Level)
            {
                case MessageLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case MessageLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Critical:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
            Console.Write($"[{prefix}: {m.TimeStamp}:{m.TimeStamp.Millisecond}]: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(m.Message + "\n");
            Console.BackgroundColor = ConsoleColor.Black;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            audioTools.StopRecording();
            pipe.Stop();
            client.DisconnectFromVoice();
            client.Dispose();
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            Console.WriteLine($"{treeView1.SelectedNode.Name}");
            try
            {
                DSharpPlus.Objects.DiscordChannel channel = client.GetChannelByID(long.Parse(treeView1.SelectedNode.Name));
                client.ConnectToVoiceChannel(channel, new DiscordVoiceConfig()
                {
                    Channels = 2,
                    OpusMode = DSharpPlus.Voice.OpusApplication.Voice,
                    SendOnly = false
                }, false, false);
                SendAudio();
            }
            catch(Exception) { }
        }

        private void Client_MentionReceived(object sender, DSharpPlus.Events.DiscordMessageEventArgs e)
        {
            if (e.Author.ID != "129303865161023488") return;
            client.DisconnectFromVoice();

            e.Channel.SendMessage($"<@{e.Author.ID}> mentioned me");
        }

        private void Client_UserSpeaking(object sender, DSharpPlus.Events.DiscordVoiceUserSpeakingEventArgs e)
        {
            Console.WriteLine($"[Speaking] {e.UserSpeaking.Username}: {e.Speaking.ToString()}");

            BeginInvoke((MethodInvoker)delegate
            {
                string image = "player" + (e.Speaking ? "_on" : "");


                treeView1.Nodes[e.Channel.Parent.ID].Nodes[e.Channel.ID].Nodes[e.UserSpeaking.ID].ImageKey = image;
                treeView1.Nodes[e.Channel.Parent.ID].Nodes[e.Channel.ID].Nodes[e.UserSpeaking.ID].SelectedImageKey = image;
            });
        }

        private void Client_VoiceStateUpdate(object sender, DiscordVoiceStateUpdateEventArgs e)
        {
            Console.WriteLine("VoiceStateUpdate");
            ResetVoiceTree();
        }

        private void Client_UserLeftVoiceChannel(object sender, DiscordLeftVoiceChannelEventArgs e)
        {
            Console.WriteLine("UserLeftVoiceChannel");
        }

        private void Client_GuildAvailable(object sender, DSharpPlus.Events.DiscordGuildCreateEventArgs e)
        {
            ResetVoiceTree();
        }

        private void Client_Connected(object sender, DiscordConnectEventArgs e)
        {
            Console.WriteLine($"Connected!{Environment.NewLine}User: {e.User.Username}#{e.User.Discriminator}");
            File.WriteAllText("conn.txt", "Im connected");
        }

        private void ResetVoiceTree()
        {
            Console.WriteLine($"Resetting VoiceTree");
            treeView1.BeginInvoke((MethodInvoker)delegate
            {
                treeView1.Nodes.Clear();

                foreach (DSharpPlus.Objects.DiscordServer server in client.GetServersList().OrderBy(x => x.ID))
                {
                    treeView1.Nodes.Add(server.ID, server.Name);
                    treeView1.Nodes[server.ID].ImageKey = "server";
                    treeView1.Nodes[server.ID].SelectedImageKey = "server";

                    foreach (DSharpPlus.Objects.DiscordChannel channel in server.Channels.Where(x => x.Type == DSharpPlus.Objects.ChannelType.Voice).OrderBy(x => x.ID))
                    {
                        treeView1.Nodes[server.ID].Nodes.Add(channel.ID, channel.Name);
                        treeView1.Nodes[server.ID].Nodes[channel.ID].ImageKey = "channel";
                        treeView1.Nodes[server.ID].Nodes[channel.ID].SelectedImageKey = "channel";
                    }

                    foreach(DSharpPlus.Objects.DiscordMember member in server.Members.Values.Where(x => x.CurrentVoiceChannel != null))
                    {
                        treeView1.Nodes[server.ID].Nodes[member.CurrentVoiceChannel.ID].Nodes.Add(member.ID, member.Username);
                        treeView1.Nodes[server.ID].Nodes[member.CurrentVoiceChannel.ID].Nodes[member.ID].ImageKey = "player";
                        treeView1.Nodes[server.ID].Nodes[member.CurrentVoiceChannel.ID].Nodes[member.ID].SelectedImageKey = "player";
                    }
                }

                treeView1.ExpandAll();
            });

            Console.WriteLine($"VoiceTree resetted");
        }

        private void SendAudio()
        {
            DiscordVoiceClient voiceClient = client.GetVoiceClient();
            audioTools.DataAvailable += (sender, e) =>
            {
                if (voiceClient?.Channel != null)
                {
                    voiceClient.SendVoice(e.Buffer);
                }
            };
            audioTools.RecordAudio(1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pipe.Stop();
        }

        private void ListenToPipe()
        {
            pipe.MessageReceived += (sender, e) =>
            {
                //string result = processGameCommand(e.Message);
                if (!e.Message.StartsWith("VERSION"))
                {
                    string channelName = client.GetChannelByID(long.Parse(channelId)).Name;
                }
            };
            BackgroundWorker bgWrk = new BackgroundWorker();
            bgWrk.DoWork += (sender, e) =>
            {
                pipe.Start();
                while(pipe.IsConnected()) { }
            };
            bgWrk.RunWorkerAsync();
        }

        public string ts_info(string arg)
        {
            return "";
        }

        public string processUnitPosition(string nickname, string connection, IrrKlang.Vector3D position, float viewAngle, bool canSpeak, bool canUseSWRadio, bool canUseLRRadio, bool canUseDDRadio, string vehicleID, int terrainInterception, float voiceVolume, float currentUnitDirection)
        {
            bool clientTalkingOnRadio = false;
            return "";
        }

        /*
        public void AddServerItem(DSharpPlus.Objects.DiscordServer server)
        {
            if (treeView1.Nodes[server.ID] == null)
                treeView1.Nodes.Add(server.ID, server.Name);
        }

        public void AddChannelItem(DSharpPlus.Objects.DiscordChannel channel)
        {
            if (treeView1.Nodes[channel.Parent.ID] == null)
                AddServerItem(channel.Parent);

            if (treeView1.Nodes[channel.Parent.ID].Nodes[channel.ID] == null)
                treeView1.Nodes[channel.Parent.ID].Nodes.Add(channel.ID, channel.Name);
        }

        public void AddUserItem(DSharpPlus.Objects.DiscordMember member)
        {
            if (treeView1.Nodes[member.CurrentVoiceChannel.Parent.ID] == null)
                AddServerItem(member.CurrentVoiceChannel.Parent);

            if (treeView1.Nodes[member.CurrentVoiceChannel.Parent.ID].Nodes[member.CurrentVoiceChannel.ID] == null)
                treeView1.Nodes[member.CurrentVoiceChannel.Parent.ID].Nodes.Add(member.CurrentVoiceChannel.ID, member.CurrentVoiceChannel.Name);

            if (treeView1.Nodes[member.CurrentVoiceChannel.Parent.ID].Nodes[member.CurrentVoiceChannel.ID].Nodes[member.ID] == null)
                treeView1.Nodes[member.CurrentVoiceChannel.Parent.ID].Nodes[member.CurrentVoiceChannel.ID].Nodes.Add(member.ID, $"{member.Username}#{member.Discriminator}");
        }

        public void ClearServerItems()
        {
            treeView1.Nodes.Clear();
        }

        public void ClearChannelItems(DSharpPlus.Objects.DiscordServer server)
        {
            if (treeView1.Nodes[server.ID] == null)
                AddServerItem(server);

            treeView1.Nodes[server.ID].Nodes.Clear();
        }

        public void ClearUserItems(DSharpPlus.Objects.DiscordChannel channel)
        {
            if (treeView1.Nodes[channel.Parent.ID] == null)
                AddServerItem(channel.Parent);

            if (treeView1.Nodes[channel.Parent.ID].Nodes[channel.ID] == null)
                AddChannelItem(channel);

            treeView1.Nodes[channel.Parent.ID].Nodes[channel.ID].Nodes.Clear();
        }

        public void RemoveUserItem(DSharpPlus.Objects.DiscordServer server, DSharpPlus.Objects.DiscordMember member)
        {
            if (treeView1.Nodes[server.ID] == null)
                treeView1.Nodes.Add(server.ID, server.Name);

            foreach(DSharpPlus.Objects.DiscordChannel channel in server.Channels.Where(x => x.Type == DSharpPlus.Objects.ChannelType.Voice))
            {
                if (treeView1.Nodes[server.ID].Nodes[channel.ID] == null)
                    treeView1.Nodes[server.ID].Nodes.Add(channel.ID, channel.Name);

                if (treeView1.Nodes[server.ID].Nodes[channel.ID].Nodes[member.ID] != null)
                    treeView1.Nodes[server.ID].Nodes[channel.ID].Nodes.RemoveByKey(member.ID);
            }
        }
        */
    }
}
