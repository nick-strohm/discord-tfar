﻿using DSharpPlus;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discord_TFAR
{
    public partial class LoginForm : Form
    {
        public bool Relogin = false;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            File.Delete("user.dat");
            Relogin = true;
            Close();
        }
        public string getSalt() { return textBox1.Text; }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Tools.LoginTools.CanLogin(textBox1.Text))
            {
                Close();
            }
        }
    }
}
