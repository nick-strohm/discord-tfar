﻿using Discord_TFAR.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Discord_TFAR
{
    public partial class Form2 : Form
    {
        public ClientData player1 = new ClientData();
        public ClientData player2 = new ClientData();

        public Form2()
        {
            InitializeComponent();

            player1.Name = "p1";
            player1.Position = new IrrKlang.Vector3D(0, 0, 0);
            player1.Rotation = new IrrKlang.Vector3D(0, 45, 0);

            player2.Name = "p2";
            player2.Position = new IrrKlang.Vector3D(3, 4.5f, 2f);
            player2.Rotation = new IrrKlang.Vector3D(0, 22, 0);

            propertyGrid1.SelectedObject = player1;
            propertyGrid2.SelectedObject = player2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            PositionTool positionTool = new PositionTool();

            IrrKlang.Vector3D position = positionTool.CalculateRelativePositionCounterClockwise(player1, player2);
            listBox1.Items.Add("Position CounterClockwise");
            listBox1.Items.Add($"X: {position.X}");
            listBox1.Items.Add($"Y: {position.Y}");
            listBox1.Items.Add($"Z: {position.Z}");
            listBox1.Items.Add("Position Clockwise");
            position = positionTool.CalculateRelativePositionClockwise(player1, player2);
            listBox1.Items.Add($"X: {position.X}");
            listBox1.Items.Add($"Y: {position.Y}");
            listBox1.Items.Add($"Z: {position.Z}");
        }
    }
}
